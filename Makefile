# Default to bookworm. These variables are duplicated in the .gitlab-ci.yml file,
# but we need them here too for local builds.
CURRENT_RELEASE?=bookworm
RELEASE?=bookworm
CI_REGISTRY_IMAGE?=ci-runner-image

ifneq ($(CI_PIPELINE_ID),)
DOCKER_TAG=$(CI_PIPELINE_ID)-$(RELEASE)
else
DOCKER_TAG=$(shell whoami)-$(RELEASE)
endif

build:
	docker build --build-arg RELEASE=$(RELEASE) --target ci-runner-image -t $(CI_REGISTRY_IMAGE):$(DOCKER_TAG) .
	docker build --build-arg RELEASE=$(RELEASE) --target ci-runner-image-dind -t $(CI_REGISTRY_IMAGE):$(DOCKER_TAG)-dind .

push:
	docker push $(CI_REGISTRY_IMAGE):$(DOCKER_TAG)
	docker push $(CI_REGISTRY_IMAGE):$(DOCKER_TAG)-dind

tag-release:
	docker tag $(CI_REGISTRY_IMAGE):$(DOCKER_TAG) $(CI_REGISTRY_IMAGE):$(RELEASE)
	docker tag $(CI_REGISTRY_IMAGE):$(DOCKER_TAG)-dind $(CI_REGISTRY_IMAGE):$(RELEASE)-dind
ifeq ($(RELEASE),$(CURRENT_RELEASE))
	docker tag $(CI_REGISTRY_IMAGE):$(DOCKER_TAG) $(CI_REGISTRY_IMAGE):latest
	docker tag $(CI_REGISTRY_IMAGE):$(DOCKER_TAG)-dind $(CI_REGISTRY_IMAGE):dind
endif

push-release:
	docker push $(CI_REGISTRY_IMAGE):$(RELEASE)
	docker push $(CI_REGISTRY_IMAGE):$(RELEASE)-dind
ifeq ($(RELEASE),$(CURRENT_RELEASE))
	docker push $(CI_REGISTRY_IMAGE):latest
	docker push $(CI_REGISTRY_IMAGE):dind
endif
