ARG RELEASE
FROM debian:${RELEASE} AS ci-runner-image

RUN apt-get update \
# Install docker-ce from docker.com
  && apt-get install -qy \
  ca-certificates \
  curl \
  && install -m 0755 -d /etc/apt/keyrings \
  && curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc \
  && chmod a+r /etc/apt/keyrings/docker.asc \
  && echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
  && apt-get update \
  && apt-get install -qy docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin \
# Install other tools
  awscli \
  expect \
  git \
  git-lfs \
  iproute2 \
  jq \
  make \
  python3-ncclient \
  s3cmd \
  sshpass \
  telnet \
  xmlstarlet \
  xsltproc \
  yq \
  && apt-get -qy autoremove \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /root/.cache

# Build the -dind image tag with extras required for docker-in-docker
FROM ci-runner-image AS ci-runner-image-dind
RUN apt-get update \
 && apt-get install -qy \
  supervisor \
  && apt-get -qy autoremove \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /root/.cache

COPY start-docker.sh entrypoint.sh /usr/local/bin/
COPY supervisor/ /etc/supervisor/conf.d/
COPY logger.sh /opt/bash-utils/logger.sh

# By defining a volume for the Docker running directory we offload these files
# to an anonymous volume, avoiding modifications to a running container.
VOLUME /var/lib/docker
ENTRYPOINT ["entrypoint.sh"]
CMD ["bash"]
